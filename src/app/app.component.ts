import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

interface MyUser {
  name?: string;
  surname?: string;
  emails?: string[];
}

interface Edits {
  user: MyUser;
  change: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'lab7';
  userForm: FormGroup;
  users: MyUser[] = [];
  edits: Edits[] = [];
  isEdit = false;
  count:number = 0;
  currentId: number = 0;

  constructor() {
    this.userForm = new FormGroup({ 
      name: new FormControl(null, [Validators.required]), 
      surname: new FormControl(null, [Validators.required]),
      emails: new FormArray([ 
        new FormControl(null, [Validators.required])
      ])
    });
  }

  clickButton(event) {
    this.currentId = event.target.id;
    this.isEdit = true;
  }

  onUserFormSubmit() {
    this.users.push(this.userForm.value);

    this.edits[this.count] = { user: this.userForm.value, change:false};
    this.count++;

    this.userForm.reset();
  }

  endChanges() {
    this.isEdit = false;
  }

  onAddEmail() {
    (<FormArray>this.userForm.controls['emails']).push(new FormControl(null, [Validators.required]));
  }

  onDelEmail(index) {
    (<FormArray>this.userForm.controls['emails']).removeAt(index);
  }

  onAddEditEmail(array) {
    (<FormArray>array.push(new FormControl(null, [Validators.required])));
  }

  onDelEditEmail(array) {
    (<FormArray>array.splice(array.length - 1, array.length - 1));
  }

  trackByFn(index: any, item: any) {
    return index;
  }
 
}
